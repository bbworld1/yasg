import setuptools

setuptools.setup(
    name="yasg",
    version="1.0.0",
    author="Vincent Wang",
    author_email="vwangsf@gmail.com",
    description="Yet Another Site Generator",
    long_description=open("README.md").read(),
    long_description_content_type="text/markdown",
    url="https://gitlab.com/bbworld1/yasg",
    download_url="",
    packages=setuptools.find_packages(),
    keywords="ssg site-generator",
    classifiers=[
        'Development Status :: 4 - Beta',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: GNU General Public License (GPL)',
        'Programming Language :: Python :: 3'
    ],
    scripts=["scripts/yasg"],
    install_requires=open("requirements.txt").read().split()
)
