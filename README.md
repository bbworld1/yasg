# yasg

Yet Another Site Generator is exactly what the name suggests - it's
yet another SSG. The goal of yasg is to be as minimal and
fast as possible, while providing a good SSG experience.

yasg is, obviously, inspired by other popular SSGs such as
Jekyll.

It has most of the features you'd expect out of an SSG:
- HTML templates
- Including templates
- Markdown content + front-matter support
- Assets/static content
- YAML data

That's all we think an SSG needs. If that sounds good to you, yasg
is a good choice. If you want more, you may want to consider another
SSG.

## Usage

HTML templates go in `templates/`.
Content goes in `content/`.
Static assets (css, images, files, etc.) go in `static/`.
Pre-build scripts (see example) go in an optional `scripts/` folder.
YAML data can go anywhere, but we recommend `data/`.

Markdown content uses "front-matter" (similar to Jekyll-style SSGs)
to specify the template to use:

```md
---
title: "Your Title"
date: 2020-12-15
author: "Java Man"
template: "main.html"
---
```

Templates can include other templates with the `{% include "other.html" %}`.

yasg uses regular old Jinja2 templates.
All HTML templates use Jinja2 syntax. If you know flask/django templating
you're good to go!

Markdown is converted the way you'd probably expect:

```
Headings (#) -> <h{1-6}>
Text -> <p>
Code -> <code>
etc.
```

You can also use HTML in your Markdown content if you need more control.

That's basically it! See `example/` for an example YASG site.

Also (shameless plug) check out [my website](https://bbworld1.gitlab.io),
which is also written in YASG.
