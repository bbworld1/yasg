#!/bin/python
import yaml
import os
from yasg.util import recursive_scan, parse_content, get_output_path

previews = {"posts": [], "template": "blog_home.html", "include": ["data/navbar.yaml"]}

for path in recursive_scan("./content/posts"):
    if os.path.isfile(path) and os.path.splitext(path)[1] in {".md"}:
        if os.path.split(path)[1] == "index.md": continue

        previews["posts"].append({})
        with open(path, "r") as f:
            frontmatter, content_raw = parse_content(f.read())

        filepath = get_output_path(os.path.splitext(path)[0], "./content", "./build")[0]
        print(filepath)


        previews["posts"][-1]["title"] = frontmatter["title"]
        previews["posts"][-1]["author"] = frontmatter["author"]
        previews["posts"][-1]["date"] = frontmatter["date"]
        previews["posts"][-1]["preview"] = " ".join(content_raw[:100].split()) + "..."
        previews["posts"][-1]["link"] = filepath

data = yaml.dump(previews, Dumper=yaml.Dumper)
data = f"""
---
{data}
---
"""

with open("content/posts/index.md", "w") as f:
    f.write(data)
