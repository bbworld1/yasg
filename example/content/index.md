---
title: "Your Epic Site"
template: "main.html"
fullspread:
  image: "main_bg.jpg"
  display: "Your Text Here"
  lead: "This is an example of YASG usage."
  buttons:
    - text: "Learn More"
      type: "btn-primary"
      link: "https://gitlab.com/bbworld1/yasg"
    - text: "Blog Posts"
      type: "btn-secondary"
      link: "/posts"
include:
  - "data/navbar.yaml"
---

# Content

Look, ma! No manually written HTML here!


