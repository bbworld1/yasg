import os
import yaml
from markdown import markdown

def recursive_scan(directory):
    for entry in os.scandir(directory):
        if entry.is_dir():
            yield from recursive_scan(entry.path)
        else:
            yield entry.path

def parse_content(raw_content: str):
    _, front_matter_raw, content_raw = raw_content.split("---")
    front_matter = yaml.load(front_matter_raw, Loader=yaml.Loader)
    front_matter.update({ "content": markdown(content_raw) })
    if front_matter.get("include") is not None:
        for datapath in front_matter["include"]:
            with open(datapath) as f:
                data = yaml.load(f.read(), Loader=yaml.Loader)
            front_matter.update(data)

    return front_matter, content_raw

def get_output_path(content_path: str, content_dir="./content", output_dir="./build"):
    output_link = os.path.relpath(content_path, content_dir) + ".html"
    output_path = os.path.join(output_dir, output_link)
    return "/" + output_link, output_path
