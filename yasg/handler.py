import os
import time
import contextlib
import watchdog
from yasg import functions
from yasg.state import options

class PausingObserver(watchdog.observers.Observer):
    def dispatch_events(self, *args, **kwargs):
        if not getattr(self, '_is_paused', False):
            super(PausingObserver, self).dispatch_events(*args, **kwargs)

    def pause(self):
        self._is_paused = True

    def resume(self):
        time.sleep(self.timeout)  # allow interim events to be queued
        self.event_queue.queue.clear()
        self._is_paused = False

    @contextlib.contextmanager
    def ignore_events(self):
        self.pause()
        yield
        self.resume()

class Handler(watchdog.events.FileSystemEventHandler):
    def __init__(self, observer, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.observer = observer

    def on_any_event(self, event):
        # TODO: Perhaps build only changes, not whole thing?
        # Although it's fast enough to rebuild a whole site every time
        if not event.is_directory:
            if event.src_path.startswith(options.template_dir) and \
                os.path.splitext(event.src_path)[1] not in {".html", ".htm", ".css", ".js"}:
                return

            self.observer.pause()
            functions.build_dev()
            self.observer.resume()
