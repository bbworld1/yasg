from yasg.state import options
from yasg.util import recursive_scan
from yasg.handler import Handler, PausingObserver
import click
from http.server import HTTPServer, SimpleHTTPRequestHandler
from markdown import markdown
import os
import time
import shutil
from jinja2 import Template, Environment, FileSystemLoader
import yaml
from watchdog.observers import Observer
from yasg import functions

@click.group()
@click.option("--template-dir", default="./templates", help="Templates folder.")
@click.option("--content-dir", default="./content", help="Content folder.")
@click.option("--static-dir", default="./static", help="Static assets folder.")
@click.option("--scripts-dir", default="./scripts", help="Pre-build scripts folder.")
@click.option("--output-dir", default="./build", help="Output folder.")
def yasg(template_dir, content_dir, static_dir, scripts_dir, output_dir):
    options.template_dir = template_dir
    options.content_dir = content_dir
    options.static_dir = static_dir
    options.scripts_dir = scripts_dir
    options.output_dir = output_dir


@yasg.command()
def build():
    functions.run_scripts()
    functions.build()

@yasg.command()
def dev():

    observer = PausingObserver()
    handler = Handler(observer)
    observer.schedule(handler, options.content_dir, recursive=True)
    observer.schedule(handler, options.static_dir, recursive=True)
    observer.schedule(handler, options.template_dir, recursive=True)

    if os.path.isdir(options.scripts_dir):
        observer.schedule(handler, options.scripts_dir, recursive=True)

    functions.run_scripts()
    functions.build()

    observer.start()
    try:
        print("Starting http server on port 8000...")
        os.chdir(options.output_dir)
        httpd = HTTPServer(('', 8000), SimpleHTTPRequestHandler)
        httpd.serve_forever()
    finally:
        observer.stop()
        observer.join()


@yasg.command()
@click.argument("directory")
def new(directory):
    os.mkdir(os.path.join(directory, "templates"))
    os.mkdir(os.path.join(directory, "content"))
    os.mkdir(os.path.join(directory, "static"))

yasg.add_command(build)
yasg.add_command(dev)
yasg.add_command(new)
