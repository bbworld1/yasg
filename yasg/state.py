class Options:
    def __init__(self):
        self.template_dir = "./templates"
        self.content_dir = "./content"
        self.static_dir = "./static"
        self.scripts_dir = "./scripts"
        self.output_dir = "./build"

options = Options()
