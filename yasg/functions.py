from yasg.state import options
from yasg.util import recursive_scan, parse_content, get_output_path
import click
from http.server import HTTPServer, SimpleHTTPRequestHandler
from markdown import markdown
import os
import time
import shutil
from jinja2 import Template, Environment, FileSystemLoader
import yaml
from watchdog.observers import Observer

def run_scripts():
    if os.path.isdir(options.scripts_dir):
        click.echo("Run pre-build scripts...")
        for path in recursive_scan(options.scripts_dir):
            os.system(path)
            click.echo(click.style("  " + path, fg="green"))

def build():
    if os.path.isdir(options.output_dir):
        click.echo("Clearing build directory...")
        shutil.rmtree(options.output_dir)
    os.mkdir(options.output_dir)

    env = Environment(
        loader=FileSystemLoader(options.template_dir),
    )

    click.echo("Building...")
    for path in recursive_scan(options.content_dir):
        filepath, ext = os.path.splitext(path)
        if ext not in {".md"}:
            continue
        with open(path, "r") as f:
            raw_content = f.read()

        front_matter, content_raw = parse_content(raw_content)

        template = env.get_template(front_matter["template"])
        html = template.render(front_matter)

        output_link, output_path = get_output_path(filepath,
                                                   options.content_dir,
                                                   options.output_dir)

        os.makedirs(os.path.dirname(output_path), exist_ok=True)
        with open(output_path, "w") as f:
            f.write(html)

        click.echo(click.style("  " + path, fg="green"))

    click.echo()
    click.echo("Copying static assets...")
    shutil.copytree(options.static_dir,
                    os.path.join(options.output_dir,
                                 options.static_dir))
    click.echo(click.style("Done!", fg="green"))

def build_dev():
    os.chdir("../")
    run_scripts()
    build()
    os.chdir(options.output_dir)
